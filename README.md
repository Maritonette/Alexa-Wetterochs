# Alexa-Wetterochs
Inoffizieller Amazon Echo/Alexa Skill für Wetterochs.  

Liest die letzte Wettermail von http://wetterochs.de/wetter/ vor.
Der Skill nutzt dazu den RSS Feed http://www.wettermail.de/wetter/current/wettermail.rss

Das Skillbild stammt von Everaldo Coelho (https://www.iconfinder.com/icons/18095/clouds_sun_weather_icon#size=256)

